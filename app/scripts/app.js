(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/login',
      'dashboard': '/dashboard',
      'movement-detail': '/detail/:account/:movement/:label',
      'help': '/help',
      'landing': '/',
      'signup': '/signup',
      'activate': '/activate/:token',
      'movement-list': '/list/:account/:label',
      'autocredit': '/autocredit'
    }
  });
}());
