import { getToken, refreshToken } from '../utils/session.js';

let fetched = false;
let result;

const { host, movements } = window.AppConfig;
const api = `${host}${movements}`;

const buildMovementDetail = (movement) => {
  const { description, date, status, accounting, establishment } = movement;
  const movementDetail = [
    description && { key: 'movement-detail.concept', value: description },
    date && { key: 'movement-detail.date', value: date },
    status && { key: 'movement-detail.description', value: status },
    establishment && { key: 'movement-detail.category', value: establishment.name },
    accounting && { key: 'movement-detail.amount', value: `${accounting.amount} ${accounting.currency}`},
    { pesos: accounting.amount },
  ].filter(Boolean);

  return movementDetail;
};

export async function getMovements(id, force = false) {
  if (fetched || fetched && !force) {
    return Promise.resolve(result);
  }

  let token = getToken();
  let myHeaders = new Headers();
  myHeaders.append('Authorization', token);

  let myRequest = new Request(api + '/' + id, {
    method: 'GET',
    headers: myHeaders
  });

  result = await fetch(myRequest).then((response) => {
    refreshToken(response);
    return response.json();
  });

  fetched = true;
  return result;
}

export async function getMovementDetail(idAccount, idMovement) {
  const movement = await getMovements(idAccount).then((moves) => {
    if (moves.result.code === 200) {
      return moves.data.movements.find((item) => item._id === idMovement);
    }
  });

  return buildMovementDetail(movement);
}

export function cleanUp() {
  fetched = false;
  result = null;
}
