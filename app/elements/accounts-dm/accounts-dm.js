import { getToken, refreshToken } from '../utils/session.js';

let fetched = false;
let result;

const { host, accounts } = window.AppConfig;
const api = `${host}${accounts}`;

export async function getAccounts() {
  if (fetched) {
    return Promise.resolve(result);
  }

  let token = getToken();
  let myHeaders = new Headers();
  myHeaders.append('Authorization', token);

  let myRequest = new Request(api, {
    method: 'GET',
    headers: myHeaders
  });

  result = await fetch(myRequest).then((response) => {
    refreshToken(response);
    return response.json();
  });
  fetched = true;

  return result;
}

export function cleanUpFetch() {
  fetched = false;
}
