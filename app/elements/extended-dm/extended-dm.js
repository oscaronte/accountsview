let fetched = false;
let result;

const { host, loans, banxico } = window.AppConfig;
const apiLoan = `${host}${loans}`;
const apiBanxico = `${host}${banxico}`;

export async function getSimulation() {
  if (fetched) {
    return Promise.resolve(result);
  }

  let myRequest = new Request(apiLoan + '/Simulation', { method: 'POST' });

  result = await fetch(myRequest).then((response) => response.json());
  fetched = true;

  return result;
}

export async function convertDollar(pesosMX) {
  if (fetched) {
    return Promise.resolve(result);
  }

  let myRequest = new Request(apiBanxico + '/dollar/' + pesosMX);

  result = await fetch(myRequest).then((response) => response.json());
  fetched = true;

  return result;
}

export function cleanUpFetch() {
  fetched = false;
}
