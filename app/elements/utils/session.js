export function setSession(token, username, client) {
  sessionStorage.setItem('token', token);
  sessionStorage.setItem('username', username);
  sessionStorage.setItem('client', client);
}

export function getUsername() {
  return sessionStorage.getItem('username');
}

export function getClient() {
  return sessionStorage.getItem('client');
}

export function getToken() {
  return sessionStorage.getItem('token');
}

export function refreshToken(response) {
  if (response) {
    let token = response.headers.get('Refresh');
    if (token) {
      sessionStorage.setItem('token', token);
    }
  }
}

export function closeSession() {
  sessionStorage.clear();
}
