let fetched = false;
let result;

export async function activateUser(token) {
  if (fetched) {
    return Promise.resolve(result);
  }

  const { host, users } = window.AppConfig;
  const endpoint = `${host}${users}/activate/${token}`;

  result = await fetch(endpoint).then((response) => response.json());
  fetched = true;
  return result;
}
