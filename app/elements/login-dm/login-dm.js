let fetched = false;
let result;
let register;

const { host, users } = window.AppConfig;
const login = `${host}${users}/login`;
const signup = `${host}${users}`;

export async function loginUser(userName, password) {
  if (fetched) {
    return Promise.resolve(result);
  }

  let params = {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    body: JSON.stringify({userName: userName, password: password})
  };

  result = await fetch(login, params).then((response) => {
    //console.log(response.headers);
    return response.json();
  });
  fetched = true;

  return result;
}

export async function signupUser(customerId, userName, password) {
  if (fetched) {
    return Promise.resolve(register);
  }
  let body = {
    userName: userName,
    password: password,
    customerId: customerId,
    activate: 'http://localhost:8001/dist/#!/activate/'
  };
  let params = {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    body: JSON.stringify(body)
  };

  register = await fetch(signup, params).then((response) => response.json());
  fetched = true;

  return register;
}

export function cleanUpFetch() {
  fetched = false;
}
