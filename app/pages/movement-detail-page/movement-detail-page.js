import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-list-simple';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-button-default';

import { getMovementDetail, cleanUp } from '../../elements/movements-dm/movements-dm.js';
import { cleanUpFetch, convertDollar } from '../../elements/extended-dm/extended-dm.js';

class MovementDetailPage extends i18n(CellsPage) {
  static get is() {
    return 'movement-detail-page';
  }

  static get properties() {
    return {
      movementTitle: { type: String },
      items: { type: Array },
      dollars: { type: Number },
      pesos: { type: Number },
      conversion: { type: Object },
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._convertModal = this.shadowRoot.querySelector('#convertModal');
  }

  constructor() {
    super();
    this.items = [];
    this.dollars = 0;
    this.pesos = 0;
  }

  onPageEnter() {
    this.subscribe('movement_title', (movementTitle) => this.movementTitle = movementTitle);
    cleanUp();
    getMovementDetail(this.params.account, this.params.movement).then((movement) => this.items = movement);
  }

  onPageLeave() {
    this.items = [];
  }

  get movementDetailItems() {
    if (!this.items.length) {
      return null;
    }

    return this.items.map(({key, value}) => {
      const localizedKey = this.t(key);

      if (localizedKey) {
        return html`
          <bbva-list-simple
            list-label="${localizedKey}">
            <span>${value}</span>
          </bbva-list-simple>
        `;
      }
    });
  }

  convertCurrency() {
    cleanUpFetch();
    this._convertModal.open();
    convertDollar(this.items[4].pesos).then((response) => {
      if (response.result.code === 200) {
        this.pesos = response.data.mxn;
        this.dollars = response.data.usd;
        this.conversion = response.data;
      } else {
        this._convertModal.close();
      }
    });

  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Volver"
            text=${this.movementTitle}
            @header-icon-left1-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          ${this.movementDetailItems ? html`${this.movementDetailItems}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
          <bbva-button-default
            @click=${this.convertCurrency}>
              Convertir En USD
          </bbva-button-default>

          <bbva-help-modal
            id="convertModal"
            header-icon="coronita:info"
            header-text="Equivalente"
            button-text="Cerrar"
            @help-modal-footer-button-click=${() => this._convertModal.close()}>
            <div slot="slot-content">
              <span>${this.pesos} MXN = ${this.dollars} USD</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
    `;
  }
}

window.customElements.define(MovementDetailPage.is, MovementDetailPage);
