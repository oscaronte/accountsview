import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-button-default';

import { activateUser } from '../../elements/activate-dm/activate-dm.js';

class ActivatePage extends i18n(CellsPage) {
  static get is() {
    return 'activate-page';
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._reiniciar = this.shadowRoot.querySelector('#reiniciar');
    this._success = this.shadowRoot.querySelector('#msgSuccess');
    this._error = this.shadowRoot.querySelector('#msgError');
    this._info = this.shadowRoot.querySelector('#msgInfo');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main>
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-notification-toast id="msgInfo" pusher="" show="">
            <br><br>
            Estamos validando su información.
            <br>
            Espere un momento, por favor.
            <br><br>
            <bbva-spinner id="spinner"></bbva-spinner>
            <br>
          </bbva-notification-toast>
          <bbva-notification-toast id="msgError" pusher="" type="error" icon-explainer="coronita:alert">
            <br><br>
            Su cuenta no pudo ser activada.
            <br>
            Probablemente el link haya caducado.
          </bbva-notification-toast>
          <bbva-notification-toast id="msgSuccess" pusher="" type="success" icon-explainer="coronita:correct">
            <br><br>
            Su cuenta ha sido activada correctamente.
            <br>
            Ahora puede ingresar a la aplicación. Su usuario y contraseña son las que definió durante el proceso de registro.
          </bbva-notification-toast>

          <bbva-button-default
            @click=${this.login}>
              Ingresar
          </bbva-button-default>

          <bbva-button-default
            @click=${this.signup}>
              Registrarse
          </bbva-button-default>

          <script type="module">
          const { fireBubblingEvents } = window.DemoHelpers;
          const toasts = document.querySelectorAll('bbva-notification-toast');
          const notBubblingEvents = ['opened', 'closed'];
          fireBubblingEvents(toasts, notBubblingEvents);
          </script>

          <bbva-help-modal
            id="reiniciar"
            header-icon="coronita:info"
            header-text="Su usuario y contraseña fueron las que definió durante su alta"
            button-text="Ir al inicio"
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  login() {
    this.navigate('login');
  }

  signup() {
    this.navigate('signup');
  }

  onPageEnter() {
    this.token = this.params.token;
    activateUser(this.token).then((result) => {
      console.log(result);
      if (result.result) {
        this._info.show = false;
        if (result.result.code === 200) {
          this._success.show = true;
        } else {
          this._error.show = true;
        }
      }
    });
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
    `;
  }

  static get properties() {
    return {
      token: { type: String },
    };
  }

}

window.customElements.define(ActivatePage.is, ActivatePage);
