import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-card';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-panel-offer';

import { getToken, getUsername, getClient } from '../../elements/utils/session.js';
import { cleanUpFetch, getAccounts } from '../../elements/accounts-dm/accounts-dm.js';

class DashboardPage extends i18n(CellsPage) {
  static get is() {
    return 'dashboard-page';
  }

  constructor() {
    super();
    this.userName = getUsername();
    this.client = getClient();
    this.accounts = [];
  }

  static get properties() {
    return {
      userName: { type: String },
      accounts: { type: Array },
      client: { type: String },
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    cleanUpFetch();
    this._logoutModal = this.shadowRoot.querySelector('#logoutModal');
    this._info = this.shadowRoot.querySelector('#msgInfo');
  }

  onPageEnter() {
    if (!this.accounts.length) {
      getAccounts().then((accounts) => {
        if (accounts.result.code === 200) {
          this.accounts = accounts.data.accounts;
          this._info.show = accounts.data.count === 0;
        } else {
          window.cells.logout();
        }
      });
    }
  }

  onPageLeave() {

  }

  handleAccountClick({ _id, product }) {
    this.publish('account_title', product);
    this.navigate('movement-list', { account: _id, label: product, });
  }

  handleOfferClick() {
    this.navigate('autocredit');
  }

  get accountList() {
    if (!this.accounts.length) {
      return null;
    }

    return this.accounts.map((account) => {
      const accountProperties = this.buildAccountProperties(account);

      return html`
        <p>
          <bbva-list-card
            ...="${spread(accountProperties)}">
          </bbva-list-card>
        </p>
      `;
    });
  }

  buildAccountProperties(account) {
    const { _id, product, balance, number } = account;

    return {
      ...(product && { 'card-title': product }),
      ...(balance && { 'amount': balance }),
      ...(number && { 'num-product': number }),
      '@click': () => this.handleAccountClick(account),
      'language': 'es',
      'currency-code': 'MXN',
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:on"
            accessibility-text-icon-left1="Cerrar Sesión"
            @header-icon-left1-click=${() => this._logoutModal.open()}
            icon-right1="coronita:help"
            accessibility-text-icon-right1="Ayuda"
            @header-icon-right1-click=${() => this.navigate('help')}
            text=${this.t('dashboard-page.header', '', { name: this.client })}>
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-notification-toast id="msgInfo" pusher="" show="">
            Sin cuentas
          </bbva-notification-toast>

          <script type="module">
          const { fireBubblingEvents } = window.DemoHelpers;
          const toasts = document.querySelectorAll('bbva-notification-toast');
          const notBubblingEvents = ['opened', 'closed'];
          fireBubblingEvents(toasts, notBubblingEvents);
          </script>

          ${this.accountList ? html`${this.accountList}` : html` `}

          <bbva-panel-offer
            class="photo horizontal dark box-look"
            content-title="Obtén un crédito para tu auto"
            link-text="¡Hazlo ahora!"
            primary-button-text="El crédito para tu auto"
            url-image="resources/images/first-car.jpg"
            @panel-offer-primary-button-click=${() => this.handleOfferClick()}>
            <p>Por ser cliente BBVA, obtén el crédito para el auto que siempre has querido.</p>
          </bbva-panel-offer>

          <bbva-help-modal
            id="logoutModal"
            header-icon="coronita:info"
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .blue {
        margin-top: 8px;
        background: #004481;
      }
    `;
  }
}

window.customElements.define(DashboardPage.is, DashboardPage);
