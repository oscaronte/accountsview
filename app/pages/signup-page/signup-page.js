import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';

import { signupUser, cleanUpFetch } from '../../elements/login-dm/login-dm.js';

class SignupPage extends i18n(CellsPage) {
  static get is() {
    return 'signup-page';
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');
    this._confirmInput = this.shadowRoot.querySelector('#confirm');
    this._customerInput = this.shadowRoot.querySelector('#customer');
    this._error = this.shadowRoot.querySelector('#msgError');
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            text="BBVA Regístrate"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-notification-toast id="msgError" pusher="" type="error" icon-explainer="coronita:alert">
            No se pudo hacer su registro.
            <br>
            Revise que el número de cliente sea el correcto.
          </bbva-notification-toast>

          <bbva-form-password
            id="customer"
            auto-validate
            validate-on-blur
            label="Cliente"
            required>
          </bbva-form-password>

          <bbva-form-field
            id="user"
            auto-validate
            validate-on-blur
            label=${this.t('login-page.user-input')}
            required>
          </bbva-form-field>

          <bbva-form-password
            id="password"
            auto-validate
            validate-on-blur
            label=${this.t('login-page.password-input')}
            required>
          </bbva-form-password>

          <bbva-form-password
            id="confirm"
            auto-validate
            validate-on-blur
            label="Confirmar"
            required>
          </bbva-form-password>

          <bbva-button-default
            @click=${this.handleValidation}>
              Registrar
          </bbva-button-default>
        </div>
      </cells-template-paper-drawer-panel>`;
  }

  handleValidation() {
    let canContinue = true;
    [this._userInput, this._passwordInput, this._customerInput, this._confirmInput].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    canContinue = canContinue && this._passwordInput.value === this._confirmInput.value;
    if (canContinue) {
      signupUser(this._customerInput.value, this._userInput.value, this._passwordInput.value).then((response) => {
        console.log(response);
        if (response.result.code === 200) {
          this.publish('activate', true);
          this.navigate('landing');
        } else {
          this.message = response.result.info;
          this.showError();
        }
        cleanUpFetch();
      });
    }
  }

  onPageEnter() {

  }

  showError() {
    this._error.show = true;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
    `;
  }

  static get properties() {
    return {
      message: { type: String },
    };
  }
}

window.customElements.define(SignupPage.is, SignupPage);
