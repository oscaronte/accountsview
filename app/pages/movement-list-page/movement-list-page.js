import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-movement';

import { getMovements, cleanUp } from '../../elements/movements-dm/movements-dm.js';

class MovementListPage extends i18n(CellsPage) {
  static get is() {
    return 'movement-list-page';
  }

  static get properties() {
    return {
      accountTitle: { type: String },
      items: { type: Array },
      account: { type: String },
    };
  }

  constructor() {
    super();
    this.items = [];
    this.account = '';
  }

  onPageEnter() {
    this._info = this.shadowRoot.querySelector('#msgInfo');
    this.subscribe('account_title', (accountTitle) => this.accountTitle = accountTitle);
    cleanUp();
    this.account = this.params.account;
    if (!this.items.length) {
      this._info.show = true;
      getMovements(this.params.account).then((movements) => {
        if (movements && movements.result && movements.result.code === 200) {
          this.items = movements.data.movements;
        } else {
          this.items = [];
        }
      });
    }
  }

  onPageLeave() {
    this.items = [];
  }

  handleMovementClick({ _id, description }) {
    this.publish('movement_title', description);
    this.navigate('movement-detail', { account: this.account, movement: _id, label: description });
  }

  get movementListItems() {
    if (!this.items.length) {
      return null;
    }

    this._info.show = this.items.length === 0;
    return this.items.map((movem) => {
      const movementProperties = this.buildMovementProperties(movem);

      return html`
        <bbva-list-movement
          ...="${spread(movementProperties)}">
        </bbva-list-movement>
        `;
    });
  }

  buildMovementProperties(movement) {
    const { _id, description, date, status, accounting } = movement;

    return {
      ...(description && { 'card-title': description }),
      ...(date && { 'date': date }),
      ...(status && { 'description': status }),
      ...(accounting && { 'amount': accounting.amount }),
      ...(accounting && { 'currency-code': accounting.currency }),
      '@click': () => this.handleMovementClick(movement),
      'local-currency': 'MXN',
      'language': 'es',
      'icon': 'coronita:auto',
      'class': 'bbva-global-semidivider',
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Volver"
            text=${this.accountTitle}
            @header-icon-left1-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-notification-toast id="msgInfo" pusher="" show="">
            <br><br>
            No se encontraron movimientos.
          </bbva-notification-toast>

          <script type="module">
          const { fireBubblingEvents } = window.DemoHelpers;
          const toasts = document.querySelectorAll('bbva-notification-toast');
          const notBubblingEvents = ['opened', 'closed'];
          fireBubblingEvents(toasts, notBubblingEvents);
          </script>

          ${this.movementListItems ? html`${this.movementListItems}` : html` `}
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
    `;
  }
}

window.customElements.define(MovementListPage.is, MovementListPage);
