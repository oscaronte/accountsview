import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';

import { cleanUp } from '../../elements/movements-dm/movements-dm.js';
import { loginUser, cleanUpFetch } from '../../elements/login-dm/login-dm.js';
import { setSession, getToken, getUsername } from '../../elements/utils/session.js';

class LoginPage extends i18n(CellsPage) {
  static get is() {
    return 'login-page';
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');
    this._error = this.shadowRoot.querySelector('#msgError');
    if (getToken('token')) {
      this.publish('user_name', getUsername());
    }
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-right1="coronita:help"
            accessibility-text-icon-right1="Sign Up"
            @header-icon-right1-click=${() => this.navigate('signup')}
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-notification-toast id="msgError" pusher="" type="error" icon-explainer="coronita:alert">
            No se pudo iniciar sesión
            <br>
            Revise los datos ingresados
          </bbva-notification-toast>

          <bbva-form-field
            id="user"
            auto-validate
            validate-on-blur
            label=${this.t('login-page.user-input')}
            required>
          </bbva-form-field>

          <bbva-form-password
            id="password"
            auto-validate
            validate-on-blur
            label=${this.t('login-page.password-input')}
            required>
          </bbva-form-password>

          <bbva-button-default
            @click=${this.handleValidation}>
              ${this.t('login-page.button')}
          </bbva-button-default>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  handleValidation() {
    let canContinue = true;
    [this._userInput, this._passwordInput].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    if (canContinue) {
      loginUser(this._userInput.value, this._passwordInput.value).then((response) => {
        if (response.result.code === 200) {
          setSession(response.data.token, this._userInput.value, response.data.client);
          this.navigate('dashboard');
        } else {
          this.showError();
          cleanUpFetch();
        }
      });
    }
  }

  onPageEnter() {
    // Cada vez que accedamos al login, simulamos una limpieza de los datos almacenados en memoria.
    cleanUpFetch();
  }

  onPageLeave() {
    // Cada vez que salgamos del login, limpiamos las cajas de texto.
    setTimeout(() => [this._userInput, this._passwordInput].forEach((el) => el.clearInput()), 3 * 1000);
  }

  showError() {
    this._error.show = true;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(LoginPage.is, LoginPage);
