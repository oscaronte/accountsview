import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-list-info';
import '@bbva-web-components/bbva-panel-offer';

import { cleanUpFetch, getSimulation } from '../../elements/extended-dm/extended-dm.js';

class AutocreditPage extends CellsPage {
  static get is() {
    return 'autocredit-page';
  }

  constructor() {
    super();
    this.loanSimulation = null;
    this.amortization = [];
    this.totalPay = 0;
  }

  static get properties() {
    return {
      loanSimulation: { type: Object },
      amortization: { type: Array },
      totalPay: { type: Number },
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__header">
        <bbva-header-main
          icon-left1="coronita:return-12"
          accessibility-text-icon-left1="Volver"
          text="El crédito para tu auto"
          @header-icon-left1-click=${() => window.history.back()}>
        </bbva-header-main>
      </div>

        <div slot="app__main" class="container">
          <bbva-panel-offer
            content-title="¡Es el momento!"
            class="photo horizontal"
            link-text="¡contrata ahora!"
            primary-button-text="Lo quiero"
            url-image="resources/images/gift-car.jpg">
            ${this.simulationInfo}
          </bbva-panel-offer>

          <table class="center">
            <thead>
              <tr>
                <th></th>
                <th>Fecha</th>
                <th>Capital</th>
                <th>Seguro</th>
                <th>Intereses</th>
                <th>IVA</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              ${this.amortizationList ? html`${this.amortizationList}` : html` `}
            </tbody>
            <tfood>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <th>${this.totalPay}</th>
            </tr>
            </tfood>
          </table>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  simulateLoan() {
    getSimulation().then((response) => {
      if (response.result.code === 200) {
        this.loanSimulation = response.data;
        var pay;

        for (let i = 0; i < this.loanSimulation.installmentPlan.scheduledPayments.length; i++) {
          pay = this.loanSimulation.installmentPlan.scheduledPayments[i];
          this.amortization[i] = {
            numPay: pay.number,
            datePay: pay.maturityDate,
            principal: pay.principal,
            fees: pay.fees,
            interest: pay.interest,
            tax: pay.tax,
            total: pay.total
          };
          this.totalPay += pay.total.amount;
        }
      }
    });
  }

  get simulationInfo() {
    if (!this.loanSimulation) {
      return null;
    }

    return html`
      <p>Monto solicitado: ${this.loanSimulation.requestedAmount.amount}</p>

      <p>Comisión por apertura: ${this.loanSimulation.fees.itemizeFees[0].feeAmount.amount}
      (${this.loanSimulation.fees.itemizeFees.percentage} %)</p>

      <p>Tasa de interés: ${this.loanSimulation.rates[0].percentage}</p>

      <p>CAT informativo: ${this.loanSimulation.rates[1].percentage}
      (calculado al: ${this.loanSimulation.rates[1].calculationDate})</p>
    `;
  }

  get amortizationList() {
    if (!this.amortization.length) {
      return null;
    }

    return this.amortization.map((amor) => {
      return html`
        <tr>
          <td>${amor.numPay}</td>
          <td>${amor.datePay}</td>
          <td style="text-align:right">${amor.principal.amount}</td>
          <td style="text-align:right">${amor.fees.amount}</td>
          <td style="text-align:right">${amor.interest.amount}</td>
          <td style="text-align:right">${amor.tax.amount}</td>
          <td style="text-align:right">${amor.total.amount}</td>
        </tr>
      `;
    });
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
  }

  onPageEnter() {
    cleanUpFetch();
    if (!this.loanSimulation) {
      this.simulateLoan();
    }
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .center {
        margin-left: auto;
        margin-right: auto;
        width: 85%;
      }

      tr:nth-child(even) {background: #CCC}
      tr:nth-child(odd) {background: #FFF}
      tfoot {color: red;}
    `;
  }
}

window.customElements.define(AutocreditPage.is, AutocreditPage);
