import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-panel-offer';

import { getToken } from '../../elements/utils/session.js';

class LandingPage extends i18n(CellsPage) {
  static get is() {
    return 'landing-page';
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._success = this.shadowRoot.querySelector('#msgSuccess');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main></bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-notification-toast id="msgSuccess" pusher="" type="success" icon-explainer="coronita:correct">
            Su cuenta ha sido registrada.
            <br>
            Revise su correo para activar su cuenta antes de poder ingresar.
          </bbva-notification-toast>

          <bbva-button-default
            @click=${this.login}>
              Ingresar
          </bbva-button-default>

          <bbva-button-default
            @click=${this.signup}>
              Registrarse
          </bbva-button-default>

          <bbva-panel-offer
            class="photo horizontal dark box-look"
            content-title="Obtén un crédito para tu auto"
            link-text="¡Hazlo ahora!"
            primary-button-text="El crédito para tu auto"
            url-image="resources/images/first-car.jpg"
            @panel-offer-primary-button-click=${() => this.handleOfferClick()}>
            <p>Por ser cliente BBVA, obtén el crédito para el auto que siempre has querido.</p>
          </bbva-panel-offer>
        </div>
      </cells-template-paper-drawer-panel>`;
  }

  login() {
    this.navigate('login');
  }

  handleOfferClick() {
    this.navigate('autocredit');
  }

  signup() {
    this.navigate('signup');
  }

  onPageEnter() {
    this.subscribe('activate', (activate) => this._success.show = activate);
  }
}

window.customElements.define(LandingPage.is, LandingPage);
